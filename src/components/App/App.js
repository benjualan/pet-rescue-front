import React from 'react';
import logo from './logo.svg';
import './App.css';
import List from '../List/List'
import Form from '../Form/Form'
import Posts from '../Posts/Posts'

const App = () => (
  <div>
    <div>
      <h2>Articles</h2>
      <List />
    </div>
    <div>
      <h2>Add a new article</h2>
      <Form />
    </div>
    <div>
      <h2>API posts</h2>
      <Posts />
    </div>
  </div>
);

export default App
